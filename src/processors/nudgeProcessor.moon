tiny = require "lib/tiny-ecs/tiny"

dump = require "lib/dump"

import
  addPoints,
  vectorBetweenPoints,
  vectorToPoint
  from require "src/pointVec"

maxIterations = 1000

nudgeProcessor = {}
nudgeProcessor.filter = tiny.requireAll "nudge", "pos", "hitbox"

nudgeProcessor.post = (entities, dt) =>
  print dump entities
  for iteration = 0, maxIterations
    for entity in *entities do
      others = [e for e in *@world.entities when @filter(self, e)]
      for other in *others do
        distVector = pointVec.vectorBetweenPoints entity.pos, other.pos
        if distVector.mag < hitbox.radius * 2
          newPos = addPoints entity.pos, vectorToPoint {
            dir: distVector.dir + math.pi*2,
            mag: distVector.mag * 2
          }
          entity.pos = newPos


nudgeProcessor
