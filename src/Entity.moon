class Entity
  new: =>
    @listeners = {}

  send: (m) =>
    [listener(m.data) for listener in *@listeners[m.msg]]

  on: (m, f) =>
    if @listeners[m] != nil
      @listeners[m][#@listeners[m] + 1] = f
    else
      @listeners[m] = { f }
