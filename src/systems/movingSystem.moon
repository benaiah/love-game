tiny = require("lib/tiny-ecs/tiny")
dump = require "lib/dump"
import vectorToPoint from require("src/pointVec")

movingSystem = tiny.processingSystem!
movingSystem.filter = tiny.requireAll "pos", "vel"

movingSystem.process = (e, dt) =>
  relativePosition = vectorToPoint(e.vel)
  e.pos =
    x: e.pos.x + relativePosition.x * dt
    y: e.pos.y + relativePosition.y * dt

movingSystem
