tiny = require "lib/tiny-ecs/tiny"
dump = require "lib/dump"

drawSystem = tiny.processingSystem!
drawSystem.filter = tiny.requireAll "pos", "draw"
drawSystem.process = (e, dt) =>
  e\draw!

spritesSystem = tiny.processingSystem!
spritesSystem.filter = tiny.requireAll "sprites"
spritesSystem.process = (e, dt) =>
  for sprite in *e\sprites! do

    spriteScale =
      x: if sprite.xScale != nil then sprite.xScale else 1
      y: if sprite.yScale != nil then sprite.yScale else 1

    spriteOrigin =
      x: if sprite.xOrigin != nil then sprite.xOrigin else 1
      y: if sprite.yOrigin != nil then sprite.yOrigin else 1

    spriteColor = if sprite.color != nil then sprite.color else { 255, 255, 255, 255 }

    love.graphics.setColor spriteColor[1], spriteColor[2], spriteColor[3], spriteColor[4]
    love.graphics.draw sprite.img,
      sprite.x, sprite.y,
      sprite.dir,
      spriteScale.x, spriteScale.y,
      spriteOrigin.x, spriteOrigin.y

{ drawSystem: drawSystem,
  spritesSystem: spritesSystem }
