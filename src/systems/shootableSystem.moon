tiny = require "lib/tiny-ecs/tiny"
dump = require "lib/dump"

messages = require "src/messages"

import vectorBetweenPoints from require "src/pointVec"

shootableSystem = tiny.processingSystem!
shootableSystem.filter = tiny.requireAll "pos", "shootable", "hitbox"

shootableSystem.process = (e, dt) =>
  filter = tiny.requireAll "pos", "projectile"
  projectiles = [entity for entity in *@world.entities when filter(self, entity)]
  for projectile in *projectiles do
    distVector = vectorBetweenPoints e.pos, projectile.pos
    -- currently assume a radius-based hitbox
    if distVector.mag < e.hitbox.radius and not projectile.hit
      e\send messages.hit projectile
      projectile\send messages.hit e

shootableSystem
