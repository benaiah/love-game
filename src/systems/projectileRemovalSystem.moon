tiny = require "lib/tiny-ecs/tiny"

projectileRemovalSystem = tiny.processingSystem!
projectileRemovalSystem.filter = tiny.requireAll "projectile", "pos", tiny.rejectAll("piercing")
projectileRemovalSystem.onAdd = (e) => e\on "hit", () ->
  @world\removeEntity e

projectileRemovalSystem
