tiny = require "lib/tiny-ecs/tiny"


healthSystem = tiny.processingSystem!
healthSystem.filter = tiny.requireAll "health"

healthSystem.onAddToWorld = (world) =>
  world.kills = 0

healthSystem.onAdd = (e) =>
  e\on "hit", () ->
    e.health -= 1

healthSystem.process = (e, dt) =>
  if e.health <= 0
    @world.kills += 1
    @world\removeEntity e

healthSystem
