tiny = require "lib/tiny-ecs/tiny"

Bullet = require "src/entities/Bullet"
Smoke = require "src/entities/Smoke"

import vectorBetweenPoints from require "src/pointVec"

pressed = false

shootingSystem = tiny.processingSystem!
shootingSystem.filter = tiny.requireAll "shoots", "pos"

shootingSystem.process = (e, dt) =>
  if love.mouse.isDown(1) and not pressed
    mX, mY = love.mouse.getPosition!
    dirVector = vectorBetweenPoints { x: e.shoots.shotPos.x, y: e.shoots.shotPos.y }, { x: mX, y: mY }
    @world\addEntity Bullet e.shoots.shotPos.x, e.shoots.shotPos.y, dirVector.dir
    @world\addEntity Smoke e.shoots.shotPos.x, e.shoots.shotPos.y
    if e.shoots.callback != nil then e.shoots.callback!
    pressed = true
  else if not love.mouse.isDown 1
    pressed = false

shootingSystem
