tiny = require "lib/tiny-ecs/tiny"

buffer = 1000

removalSystem = tiny.processingSystem!
removalSystem.filter = tiny.requireAll "pos"

removalSystem.process = (e, dt) =>
  dX, dY = love.graphics.getDimensions!
  if e.pos.x < -1 * buffer or
    e.pos.x > dX + buffer or
    e.pos.y < -1 * buffer or
    e.pos.y > dY + buffer
    @world\removeEntity e

removalSystem
