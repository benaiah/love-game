tiny = require "lib/tiny-ecs/tiny"

buffer = 1000

removeDistantEntitiesSystem = tiny.processingSystem!
removeDistantEntitiesSystem.filter = tiny.requireAll "pos"

removeDistantEntitiesSystem.process = (e, dt) =>
  dX, dY = love.graphics.getDimensions!
  if e.pos.x < -1 * buffer or
    e.pos.x > dX + buffer or
    e.pos.y < -1 * buffer or
    e.pos.y > dY + buffer
    @world\removeEntity e

removeDistantEntitiesSystem
