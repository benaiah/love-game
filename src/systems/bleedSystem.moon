tiny = require "lib/tiny-ecs/tiny"

Blood = require "src/entities/Blood"

bleedSystem = tiny.processingSystem!
bleedSystem.filter = tiny.requireAll "pos", "bleeds"
bleedSystem.onAdd = (e) => e\on "hit", () ->
  @world\addEntity Blood e.pos.x, e.pos.y

bleedSystem
