tiny = require "lib/tiny-ecs/tiny"

import
  cardinalDirections
  pointToVector
  vectorToPoint
  from require "src/pointVec"

keyboardMovementSystem = tiny.processingSystem!
keyboardMovementSystem.filter = tiny.requireAll "dir_binds", "vel", "speed"
keyboardMovementSystem.process = (e, dt) =>
  pointTo = { x: 0, y: 0 }
  moving = false
  if love.keyboard.isDown( e.dir_binds.up )
    moving = true
    pointTo.y -= 1

  if love.keyboard.isDown e.dir_binds.down
    moving = true
    pointTo.y += 1

  if love.keyboard.isDown e.dir_binds.left
    moving = true
    pointTo.x -= 1

  if love.keyboard.isDown e.dir_binds.right
    moving = true
    pointTo.x += 1

  vec = pointToVector pointTo

  e.vel.dir = vec.dir
  e.vel.mag = if moving then e.speed * dt * 1000 else 0

keyboardMovementSystem
