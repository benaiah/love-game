tiny = require "lib/tiny-ecs/tiny"

fadeSystem = tiny.processingSystem!
fadeSystem.filter = tiny.requireAll "fade"

fadeSystem.process = (e, dt) =>

  if e.fade.startingOpacity == nil
    e.fade.startingOpacity = 255

  if e.fade.timeLeft == nil
    e.fade.timeLeft = e.fade.time

  e.fade.opacity = (e.fade.timeLeft / e.fade.time) * e.fade.startingOpacity
  e.fade.timeLeft -= dt

  if e.fade.opacity <= 0
    @world\removeEntity e

fadeSystem
