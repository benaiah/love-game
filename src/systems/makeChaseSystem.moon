tiny = require "lib/tiny-ecs/tiny"
import vectorBetweenPoints from require "src/pointVec"

makeChaseSystem = (chased) ->
  chaseSystem = tiny.processingSystem!
  chaseSystem.filter = tiny.requireAll "pos", "vel", "chases"

  chaseSystem.process = (e, dt) =>
    dirVec = vectorBetweenPoints e.pos, chased.pos
    e.vel.dir = dirVec.dir

  chaseSystem

makeChaseSystem
