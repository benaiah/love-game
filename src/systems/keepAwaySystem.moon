tiny = require "lib/tiny-ecs/tiny"

import
  addPoints,
  vectorBetweenPoints,
  vectorToPoint
  from require "src/pointVec"

keepAwaySystem = tiny.processingSystem!
keepAwaySystem.filter = tiny.requireAll "keepAway", "pos", "hitbox"

-- maximum number of times per frame the system will try to move
-- entities apart per entity
maxIterations = 100
iterations = maxIterations

keepAwaySystem.process = (e, dt) =>
  -- get all other entities matching the filter
  others = [entity for entity in *@world.entities when @filter(self, entity)]
  for i = maxIterations
    for other in *others do
      distVector = pointVec.vectorBetweenPoints e.pos, other.pos
      if distVector.mag < hitbox.radius * 2
        newPos = addPoints e.pos, vectorToPoint {
          dir: distVector.dir + math.pi*2,
          mag: distVector.mag
        }
        e.pos = newPos

keepAwaySystem
