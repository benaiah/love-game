tiny = require "lib/tiny-ecs/tiny"

drawCircleSystem = tiny.processingSystem!
drawCircleSystem.filter = tiny.requireAll "pos", "drawCircle"
drawCircleSystem.process = (e, dt) =>
  love.graphics.setColor e.drawCircle.color
  love.graphics.circle e.drawCircle.mode, e.pos.x, e.pos.y, e.drawCircle.radius

drawCircleSystem
