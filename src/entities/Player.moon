dump = require "lib/dump"

assets = require "src/assets"
Entity = require "src/Entity"
import
  vectorToPoint
  vectorBetweenPoints
  rotateRadiansByDegrees
  addPoints
  from require "src/pointVec"

getTriangleSideLengthFromRadius = (r) -> r * math.sqrt 3

class Player extends Entity
  new: =>
    super!

    @pos =
      x: 0
      y: 0
    @radius = 8
    @hitbox =
      shape: "circle"
      radius: @radius
    @vel =
      dir: 0
      mag: 0
    @speed = 10
    @dir_binds =
      up: "w"
      left: "a"
      down: "s"
      right: "d"
    @shoots =
      callback: ->
        love.audio.play love.audio.newSource assets.snd.shoot
        @shoots.whichGun = if @shoots.whichGun == "left" then "right" else "left"
      shotPos:
        x: @x
        y: @y
      whichGun: "left"
    @sprites = =>
      radius = @radius
      side = getTriangleSideLengthFromRadius radius
      sprites = {}

      mX, mY = love.mouse.getPosition!
      vecToMouse = vectorBetweenPoints { x: @pos.x, y: @pos.y }, { x: mX, y: mY }
      point1 = addPoints @pos, vectorToPoint { dir: vecToMouse.dir, mag: radius }

      spriteDir = rotateRadiansByDegrees vecToMouse.dir, 90

      bodySprite =
        img: assets.img.player,
        x: @pos.x, y: @pos.y,
        dir: spriteDir,
        xOrigin: 8, yOrigin: 8

      -- get drawing point of left/right guns
      leftGunPoint = addPoints @pos, vectorToPoint {
        dir: rotateRadiansByDegrees vecToMouse.dir, 270
        mag: 12
      }
      rightGunPoint = addPoints @pos, vectorToPoint {
        dir: rotateRadiansByDegrees vecToMouse.dir, 90
        mag: 12
      }

      -- left gun
      leftGunSprite =
        img: assets.img.gun,
        x: leftGunPoint.x, y: leftGunPoint.y,
        dir: spriteDir,
        xOrigin: 4, yOrigin: 4

      -- right gun
      rightGunSprite =
        img: assets.img.gun,
        x: rightGunPoint.x, y: rightGunPoint.y,
        dir: spriteDir,
        xScale: -1,
        xOrigin: 4, yOrigin: 4

      -- Set shot position to the end of the triangle
      if @shoots.whichGun == "left" then
        @shoots.shotPos.x = leftGunSprite.x
        @shoots.shotPos.y = leftGunSprite.y
      else
        @shoots.shotPos.x = rightGunSprite.x
        @shoots.shotPos.y = rightGunSprite.y

      { bodySprite, leftGunSprite, rightGunSprite }
