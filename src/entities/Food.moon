Entity = require "src/Entity"

class Food extends Entity
  new: =>
    super!

    @pos =
      x: math.random! * love.graphics.getWidth!
      y: math.random! * love.graphics.getHeight!
    @vel =
      mag: 50
      dir: 0
    @chases = true
    @drawCircle =
      color: { 200, 100, 100 }
      radius: 4
      mode: "fill"
