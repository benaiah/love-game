assets = require "src/assets"
Entity = require "src/Entity"

class Blood extends Entity
  new: (x, y) =>
    super!

    @pos = { :x, :y }
    @fade =
      time: 1

    rotation = 0
    @sprites = =>
      {
        {
          img: assets.img.blood
          x: @pos.x
          y: @pos.y
          dir: rotation
          color: {255, 255, 255, @fade.opacity}
          xOrigin: 8
          yOrigin: 8
        },
      }
