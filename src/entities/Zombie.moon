assets = require "src/assets"
Entity = require "src/Entity"

import vectorBetweenPoints, rotateRadiansByDegrees from require "src/pointVec"

class Zombie extends Entity
  new: (x, y) =>
    super!

    @pos = { :x, :y }
    @vel =
      dir: 0
      mag: 80
    @chases = true
    @shootable = true
    @bleeds = true
    @countedForKills = false
    @nudge = true
    @health = 3
    @killsOnTouch = true
    @hitbox =
      shape: "circle"
      radius: 10
    @sprites = =>
      spriteDir = rotateRadiansByDegrees @vel.dir, 90
      {
        {
          img: assets.img.zombie
          x: @pos.x
          y: @pos.y
          xOrigin: 8
          yOrigin: 8
          dir: spriteDir,
        },
      }
