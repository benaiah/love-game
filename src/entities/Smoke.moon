assets = require "src/assets"
Entity = require "src/Entity"

class Smoke extends Entity
  new: (x, y) =>
    super!

    @pos = { :x, :y }
    @fade =
      time: 0.2
      startingOpacity: 100

    rotation = math.random! * math.pi * 2
    img = assets.img.randomSmoke!
    @sprites = =>
      return {
        {
          img: img
          x: @pos.x
          y: @pos.y
          dir: rotation
          color: {255, 255, 255, @fade.opacity}
          xOrigin: 190.5
          yOrigin: 173
          xScale: .05
          yScale: .05
        },
      }
