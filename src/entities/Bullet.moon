Entity = require "src/Entity"

class Bullet extends Entity
  new: (x, y, dir) =>
    super!

    @pos = { x: x, y: y }
    @vel = { :dir, mag: 1000 }
    @projectile = true
    @drawCircle =
      color: { 255, 0, 0 }
      radius: 2
      mode: "fill"
