assets =
  img:
    player: love.graphics.newImage "assets/img/player.png"
    gun: love.graphics.newImage "assets/img/gun.png"
    zombie: love.graphics.newImage "assets/img/zombie.png"
    blood: love.graphics.newImage "assets/img/blood.png"
    smoke: [love.graphics.newImage("assets/img/smoke/" .. item) for item in *love.filesystem.getDirectoryItems("assets/img/smoke")]
  snd:
    shoot: "assets/snd/shoot.wav"

assets.img.randomSmoke = ->
  assets.img.smoke[math.random( #assets.img.smoke )]

assets
