cardinalDirections =
  right: 0
  left: math.pi
  down: math.pi/2
  up: math.pi + math.pi/2

rotateRadiansByDegrees = (rad, deg) ->
  rad + math.rad(deg)

addPoints = (point_a, point_b) ->
  {
    x: point_a.x + point_b.x
    y: point_a.y + point_b.y
  }

-- A vector is a table with two keys, 'dir' and 'mag'
vectorToPoint = (vec) ->
  {
    x: math.cos(vec.dir) * vec.mag,
    y: math.sin(vec.dir) * vec.mag
  }

vectorBetweenPoints = (point_a, point_b) ->
  -- if point_a.x == 0 and point_b.x == 0
  --   return {
  --     dir: if point_b.y < 0 then cardinalDirections.up else cardinalDirections.down,
  --     mag: math.abs(point_b.y)
  --   }
  -- if point_a.y == 0 and point_b.y == 0
  --   return {
  --     dir: if point_b.x < 0 then cardinalDirections.left else cardinalDirections.right,
  --     mag: math.abs(point_b.x)
  --   }

  {
    dir: math.atan2(point_b.y - point_a.y, point_b.x - point_a.x),
    mag: math.sqrt(
      math.abs(point_b.y - point_a.y)^2 +
      math.abs(point_b.x - point_a.x)^2)
  }

pointToVector = (point) ->
  vectorBetweenPoints({x: 0, y: 0}, point)

{
  :cardinalDirections, :vectorToPoint, :pointToVector,
  :vectorBetweenPoints, :rotateRadiansByDegrees, :addPoints
}
