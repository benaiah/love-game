# Untitled Game in Löve2D

This is a small game written in [MoonScript](https://moonscript.org/)
using the [Love2d](https://love2d.org/) game framework.

## Architecture

The game uses the [tiny-ecs](https://github.com/bakpakin/tiny-ecs)
library for simple entity-component-system structuring. This library
fundamentally shapes how the project is structured, and is quite
simple - a quick read of its documentation will make the code here
much clearer.

While entities in tiny-ecs are simply key-value stores, there are some
explicit entities contained in `src/entities` which are used to create
a few of the standard entities.

In addition to the standard ECS constructions, there's also a concet
of *messages*. Messages exist because standard ECS systems are awkward
for certain purposes, particularly events that need to affect more
than one entity. However, we don't want to start attaching
functionality directly to entities, or our entities will become less
useful as data and have fewer opportunities for systems to manipulate
them arbitrarily. To solve this, we have our entities inherit from
Entity, which attaches three things to each entity:

- a `@listeners` table, which is used as a dictionary of
  `message name` -> `array of message handlers`.
- a `send` method, which, given a message `m`, simply passes `m.data`
  to each listener in the sub-array of `@listeners` corresponding to
  the string `m.msg`.
- an `on` method, which, given a message string and a function,
  adds the function to the subarray of `@listeners` corresponding to
  the message string.

This ends up being very simple. For example:

```moonscript
-- Create an entity
entity = Entity!

entity.health = 10

entity\on "hit", (data) ->
  e.health -= data.damage
  
entity\send {
  msg: "hit",
  data: {
    damage: 1
  }
}

-- entity.health is now 9
```

This might seem like it causes implicit interdependencies, as we're
attaching message handlers to entities outside of a system, and so our
functionality is now split between these handlers and the systems
themselves. The trick is, we never manually add these listeners to
entities - we add them in the `<tiny-ecs system>.onAdd` function.
Since we can have an arbitrary number of listeners for each message,
the systems don't have to worry about each other, and can react to
actions that other systems took without knowing about the other
systems. It also means that message handlers will be attached to every
entity that system is using, so we don't have to try to keep them in
sync manually.

In order to keep messages tidy and easy to modify from one place,
every message is created using one of the message-creator function in
`src/messages`.

TODO: Both tiny-ecs systems and message handlers should work on a diff
of the previous frame to the next instead of modifying the existing
entity. This would make system/handler ordering bugs much less likely
and easier to fix, by restricting the entities to one state per frame.
