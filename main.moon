tiny = require "lib/tiny-ecs/tiny"
dump = require "lib/dump"

assets = require "src/assets"

pointVec = require "src/pointVec"
Player = require "src/entities/Player"
Food = require "src/entities/Food"
Zombie = require "src/entities/Zombie"

movingSystem                    = require "src/systems/movingSystem"
{ :drawSystem, :spritesSystem } = require "src/systems/drawSystem"
drawCircleSystem                = require "src/systems/drawCircleSystem"
keyboardMovementSystem          = require "src/systems/keyboardMovementSystem"
shootingSystem                  = require "src/systems/shootingSystem"
shootableSystem                 = require "src/systems/shootableSystem"
makeChaseSystem                 = require "src/systems/makeChaseSystem"
healthSystem                    = require "src/systems/healthSystem"
bleedSystem                     = require "src/systems/bleedSystem"
fadeSystem                      = require "src/systems/fadeSystem"
removeDistantEntitiesSystem     = require "src/systems/removeDistantEntitiesSystem"
projectileRemovalSystem         = require "src/systems/projectileRemovalSystem"

nudgeProcessor = require "src/processors/nudgeProcessor"

player = Player!

chasePlayerSystem = makeChaseSystem player

world = tiny.world movingSystem,
  removalSystem,
  projectileRemovalSystem,
  fadeSystem,
  chasePlayerSystem,
  drawSystem,
  drawCircleSystem,
  spritesSystem,
  shootableSystem,
  keyboardMovementSystem,
  shootingSystem,
  killsSystem,
  bleedSystem,
  healthSystem,
  removeDistantEntitiesSystem,
  player

world.processors = {
  nudgeProcessor,
}

for processor in *world.processors do
  processor.world = world

makeFood = ->
  world\addEntity Food!

makeZombie = ->
  world\addEntity Zombie math.random! * love.graphics.getWidth!, math.random! * love.graphics.getHeight!

drawCursor = (x, y, color) ->
  love.graphics.setColor color[1], color[2], color[3]
  love.graphics.line x, y-12, x, y-3
  love.graphics.line x, y+12, x, y+3
  love.graphics.line x-12, y, x-3, y
  love.graphics.line x+12, y, x+3, y

love.load = ->
  player.pos.x = love.graphics.getWidth! / 2
  player.pos.y = love.graphics.getHeight! / 2
  love.mouse.setVisible false
  love.graphics.setDefaultFilter "nearest", "nearest", 0
  love.graphics.setBackgroundColor 200, 200, 200

love.keypressed = (key, scanCode, isRepeat) ->
  if key == "f" and not isRepeat
    makeFood!

  if key == "z" and not isRepeat
    makeZombie!

  if key == "escape"
    love.event.quit!

measurement = 0

round = (num, idp) ->
  mult = 10^(idp or 0)
  math.floor(num * mult + 0.5) / mult

love.draw = ->
  dt = love.timer.getDelta!

  for processor in *world.processors do
    if processor.pre
      entities = [entity for entity in *world.entities when processor.filter(nil, entity)]
      processor\pre entities, dt

  -- Check for player death
  filter = tiny.requireAll "killsOnTouch", "pos"
  killers = [entity for entity in *world.entities when filter(nil, entity)]
  for killer in *killers do
    distVector = pointVec.vectorBetweenPoints player.pos, killer.pos
    if distVector.mag < 24
      love.event.quit!

  mX, mY = love.mouse.getPosition!
  drawCursor mX, mY, {0, 0, 255}

  if round(math.random!, 2) * 10 == 1 and world\getEntityCount! < 100
    makeZombie!

  smoothing = 0.9 -- larger=more smoothing
  measurement = (measurement * smoothing) + (dt * (1 - smoothing))

  love.graphics.print "fps: #{round(1 / measurement, 0)}", 0, 0
  love.graphics.print "entities: #{world\getEntityCount!}", 0, 24
  love.graphics.print "kills: #{if world.kills then world.kills else 0}", 0, 48

  world\update dt

  for processor in *world.processors do
    if processor.post
      entities = [entity for entity in *world.entities when processor.filter(nil, entity)]
      processor\post entities, dt
